defmodule UploaderCliTest do
  use ExUnit.Case
  doctest UploaderCli

  #  NOTE: The CLI test suite is lacking

  describe "cli flow" do
    test "flow test" do
      args = [
        "up",
        "--name=package_name_1",
        "--version=1.0.0",
        "--file=package_name_1_v_1_0_0",
        "--dir=./test_packages"
      ]

      UploaderCli.main(args)

      args = [
        "down",
        "--name=package_name_1",
        "--dir=./downloads"
      ]

      UploaderCli.main(args)
    end
  end
end

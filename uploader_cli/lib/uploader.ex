defmodule UploaderCli.Uploader do
  @uploader_api_url Application.get_env(:uploader_cli, :uploader_api_url)
  @uploader_api_packages_endpoint @uploader_api_url <> "/packages"

  defp do_get(url),
       do:
         with { :ok, %{ body: body } } <- HTTPoison.get(url, get_headers()),
         do:
           Jason.decode!(body)

  defp do_post(url, body),
       do:
         with { :ok, %{ body: body } } <- HTTPoison.post(
           url,
           Jason.encode!(body),
           get_headers(),
           [ ]
         ),
         do:
           Jason.decode!(body)

  defp do_post(url, body, :no_decode),
       do:
         with { :ok, %{ body: body } } <- HTTPoison.post(
           url,
           Jason.encode!(body),
           get_headers(),
           [ ]
         ),
         do:
           :ok

  defp do_delete(url),
       do:
         with { :ok, %{ body: body } } <- HTTPoison.delete(url, get_headers()),
         do:
           Jason.decode!(body)

  defp get_headers, do: [ { "Content-type", "application/json" }, { "x-dummy-api-key", "i_am_a_dummy" } ]

  def search_packages(name_kernel), do: do_get(@uploader_api_packages_endpoint <> "/?name=#{name_kernel}")[ "data" ]

  def list_package_versions(name), do: do_get(@uploader_api_packages_endpoint <> "/#{name}")[ "data" ]
  def show_package(name, version), do: do_get(@uploader_api_packages_endpoint <> "/#{name}/#{version}")[ "data" ]

  def delete_package(id), do: do_delete(@uploader_api_packages_endpoint <> "/#{id}")
  def delete_package(name, version),
      do:
        with %{ id: package_id } <- show_package(name, version),
        do:
          delete_package(package_id)

  def upload_package({ name, description, version, filename, content }) do
    with encoded_content <- Base.encode64(content),
         response <- do_post(
           @uploader_api_packages_endpoint,
           %{
             package: %{
               name: name,
               description: description,
               version: version,
               filename: filename
             }
           }
         ),
         %{ "id" => package_id } <- response[ "data" ]
      do

      do_post(
        @uploader_api_packages_endpoint <> "/upload",
        %{
          package_id: package_id,
          content: encoded_content
        },
        :no_decode
      )
    end
  end

  def download_package(name, dir_path), do: download_package(name, "latest", dir_path)
  def download_package(name, version, dir_path) do
    with %{ "id" => package_id, "filename" => filename } <- show_package(name, version),
         %{ "content" => content } <- do_post(
           @uploader_api_packages_endpoint <> "/download",
           %{
             package_id: package_id
           }
         ),
         decoded_content <- Base.decode64!(content)
      do

      IO.puts dir_path
              |> Path.absname()
              |> Path.join(filename)
      IO.puts content

      dir_path
      |> Path.absname()
      |> Path.join(filename)
      |> File.write(decoded_content)
    end
  end
end

defmodule UploaderCli do
  @moduledoc """
  Documentation for UploaderCli.
  """

  alias UploaderCli.Uploader, as: PackageManager

  def main(args), do: handle_args(args)

  def handle_args([ "search", "--name=" <> name_kernel ]),
      do: print_packages(PackageManager.search_packages(name_kernel))

  def handle_args([ "list", "--name=" <> name ]), do: print_packages(PackageManager.list_package_versions(name))
  def handle_args([ "list", "--name=" <> name, "--version=" <> version ]),
      do: print_package(PackageManager.show_package(name, version))

  def handle_args([ "down", "--name=" <> name, "--dir=" <> dir_path ]),
      do: IO.inspect PackageManager.download_package(name, dir_path)
  def handle_args([ "down", "--name=" <> name, "--version=" <> version, "--dir=" <> dir_path ]),
      do: IO.inspect PackageManager.download_package(name, version, dir_path)

  def handle_args(
        [
          "up",
          "--name=" <> name = n,
          "--version=" <> version = v,
          "--file=" <> filename = f,
          "--dir=" <> dirname = d
        ]
      ), do: handle_args([ "up", n, v, "--description=", f, d ])
  def handle_args(
        [
          "up",
          "--name=" <> name,
          "--version=" <> version,
          "--description=" <> description,
          "--file=" <> filename,
          "--dir=" <> dirname
        ] = details
      ) do
    with path <- dirname
                 |> Path.absname()
                 |> Path.join(filename),
         content <- path
                    |> File.read!()
      do

      case PackageManager.upload_package({ name, description, version, filename, content }) do
        :ok -> IO.puts "Package [#{name}@#{version}] uploaded successfully"
        _ -> IO.puts "Unable to upload file at this time."
      end
    end
  end

  def handle_args([ "help" ]) do
    IO.puts "\n----------\n"
    IO.puts "USAGE:"

    IO.puts "\n./uploader_cli help"
    IO.puts "\tprints this message"

    IO.puts "\n./uploader_cli search --name=<search_target>"
    IO.puts "\treturns all packages with <search_target> in the name"

    IO.puts "\n./uploader_cli list --name=<package_name>"
    IO.puts "\treturns all versions of the package named <package_name>"

    IO.puts "\n./uploader_cli list --name=<package_name> --version=<package_version>"
    IO.puts "\treturns a specific package with corresponding name and version"

    IO.puts "\n./uploader_cli list --name=<package_name> --version=latest"
    IO.puts "\treturns the latest version of the package with name <package_name>"

    IO.puts "\n./uploader_cli up --name=<package_name> --version=<package_version> --filename=<package_file_path> [--description=<package_description]"
    IO.puts "\treturns uploads a new package with the given details"

    IO.puts "\n./uploader_cli down --name=<package_name> --dir=<local_location>"
    IO.puts "\tdownloads the latest package named <package_name> to the specific directory"

    IO.puts "\n./uploader_cli down --name=<package_name> --version=<package_version> --dir=<local_location>"
    IO.puts "\tdownloads the specific version of package named <package_name> to the specific directory"
  end

  defp print_packages(packages), do: Enum.each(packages, fn p -> print_package(p) end)
  defp print_package(%{ "description" => description, "id" => id, "name" => name, "version" => version }) do
    IO.puts "----------"
    IO.puts "Info: #{name}@#{version} (#{id})"
    IO.puts "Description: #{description}\n"
  end
end

defmodule UploaderCli.MixProject do
  use Mix.Project

  def project do
    [
      app: :uploader_cli,
      version: "0.1.0",
      elixir: "~> 1.7",
      start_permanent: Mix.env() == :prod,
      deps: deps(),
      escript: escript()
    ]
  end

  # Run "mix help compile.app" to learn about applications.
  def application do
    [
      extra_applications: [ :logger ]
    ]
  end

  # Run "mix help deps" to learn about dependencies.
  defp deps do
    [
      { :httpoison, "~> 1.3.0" },
      { :jason, "~> 1.1.1" }
    ]
  end

  defp escript do
    [ main_module: UploaderCli ]
  end
end

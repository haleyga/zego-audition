# UploaderCli

**TODO: Add description**

## Build
```bash
mix escript.build
```

## Usage

NOTE: command line argument order matters!! (I know, I'm terrible.)

### Search

Search for packages containing "some_name_fragment" in the their names.
```bash
./uploader_cli search --name=some_name_fragment
```

### List

List all versions of a package with the name "some_name"
```bash
./uploader_cli list --name=some_name
```

List the specific version of a package with the name "some_name" and version "1.0.0"
```bash
./uploader_cli list --name=some_name --version=1.0.0
```

List the latest version of a package with the name "some_name"
```bash
./uploader_cli list --name=some_name --version=latest
```

### Upload

Uploads a new package with the given details. 

```name``` the name of the package

```version``` the new version (if the name/version combination already exists, no upload occurs)

```dir``` can be a relative path

```file``` is the name that will be used when downloading the package.

```description``` is optional
```bash
./uploader_cli up --name=some_name --version=1.0.0 --dir=./test_packages --file=package_name_1_v_1_0_0.tgz
```

### Download

#### latest

Downloads the latest version of a package with the requested name. 

```name``` the name of the package

```dir``` can be a relative path to a download directory

```bash
./uploader_cli down --name=some_name --dir=./downloads
```

#### specific 

Downloads a specific version of a package with the requested name and version. 

```name``` the name of the package

```version``` requested version

```dir``` can be a relative path to a download directory

```bash
./uploader_cli down --name=some_name --version=1.0.0 --dir=./downloads
```

# Zego Audition

A simple RESTful upload tool with CLI-driven demo app.

## Questions
                                                      
### API Service

Write a RESTful API service in language, datastore & framework of your choice for a package download service like npmjs.org.
Below are some more details:

- User should be authenticated for each API call. Don't worry about adding authorization flow for now. Just hard-code some sort of logic on either code or datastore when user sends authorization information as part of the api call.
- User should be able to list all the available packages in the system
- User should be able to search the packages via name.
- User should be able to see individual package information. User can also request for specific version of the package as part of API call
- User should be able to upload/download each version of package. Once uploaded, that particular version of package can not be changed by user.
- Try to keep this system as simple as possible while getting the basic functionality. Building data schema and system design is up to you.
- Do not worry about authorization strategies. Assume all the registered users can publish new versions of any packages in the system.

### Your new package manager CLI client

Write a CLI client for the package management service built on first task.
It must be able to perform publishing, searching and installing packages.

## Notes

- writing unit tests, using a single coding style, appropriate documentations, etc. are basic expectations
- you're expected to submit source code repository in github/bitbucket (or other cloud git services)
- try to build up your source code with feature based commits
- feature completion is not the number 1 priority, decent codebase with partially completed features will be better than fully completed unmanageable codebase

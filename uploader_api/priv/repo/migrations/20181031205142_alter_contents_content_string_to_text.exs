defmodule UploaderApi.Repo.Migrations.AlterContentsContentStringToTest do
  use Ecto.Migration

  def change do
    alter table(:contents) do
      modify :content, :text
    end
  end
end

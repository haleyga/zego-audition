defmodule UploaderApi.Repo.Migrations.RenamePackagesContentToPath do
  use Ecto.Migration

  def change do
    rename table(:packages), :content, to: :path
  end
end

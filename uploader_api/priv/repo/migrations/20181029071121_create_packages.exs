defmodule UploaderApi.Repo.Migrations.CreatePackages do
  use Ecto.Migration

  def change do
    create table(:packages) do
      add :name, :string
      add :version, :string
      add :filename, :string
      add :description, :string
      add :content, :string

      timestamps()
    end

    create unique_index(:packages, [ :name, :version ], name: :packages_name_version_unique_index)
  end
end

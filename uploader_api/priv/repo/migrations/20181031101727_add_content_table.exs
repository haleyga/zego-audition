defmodule UploaderApi.Repo.Migrations.AddContentTable do
  use Ecto.Migration

  def change do
    create table(:contents) do
      add :package_id, references(:packages)
      add :content, :string

      timestamps()
    end

    create unique_index(:contents, :package_id)
  end
end

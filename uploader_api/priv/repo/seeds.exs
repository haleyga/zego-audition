# Script for populating the database. You can run it as:
#
#     mix run priv/repo/seeds.exs
#
# Inside the script, you can read and write to any of your
# repositories directly:
#
#     UploaderApi.Repo.insert!(%UploaderApi.SomeSchema{})
#
# We recommend using the bang functions (`insert!`, `update!`
# and so on) as they will fail if something goes wrong.

defmodule Const do
  @storage_bucket Application.get_env(:uploader_api, :storage_bucket)
  def storage_bucket, do: @storage_bucket
end

package = UploaderApi.Repo.insert!(
  %UploaderApi.Packages.Package{
    #    path: Const.storage_bucket,
    filename: "some_filename",
    description: "some description",
    name: "name1",
    version: "1.0.0"
  }
)
UploaderApi.Repo.insert!(
  %UploaderApi.Packages.Content{
    package_id: package.id,
    content: "binary_stuff_for_package_one"
  }
)

package = UploaderApi.Repo.insert!(
  %UploaderApi.Packages.Package{
    #    path: Const.storage_bucket,
    filename: "some_filename",
    description: "some description",
    name: "name1",
    version: "1.1.0"
  }
)
UploaderApi.Repo.insert!(
  %UploaderApi.Packages.Content{
    package_id: package.id,
    content: "binary_stuff_for_package_two"
  }
)

package = UploaderApi.Repo.insert!(
  %UploaderApi.Packages.Package{
    #    path: Const.storage_bucket,
    filename: "some_filename",
    description: "some description",
    name: "name1",
    version: "2.0.0"
  }
)
UploaderApi.Repo.insert!(
  %UploaderApi.Packages.Content{
    package_id: package.id,
    content: "binary_stuff_for_package_three"
  }
)

package = UploaderApi.Repo.insert!(
  %UploaderApi.Packages.Package{
    #    path: Const.storage_bucket,
    filename: "some_filename",
    description: "some description",
    name: "name2",
    version: "0.0.1"
  }
)
UploaderApi.Repo.insert!(
  %UploaderApi.Packages.Content{
    package_id: package.id,
    content: "binary_stuff_for_package_four"
  }
)

package = UploaderApi.Repo.insert!(
  %UploaderApi.Packages.Package{
    #    path: Const.storage_bucket,
    filename: "some_filename",
    description: "some description",
    name: "name3",
    version: "1.0.0"
  }
)
UploaderApi.Repo.insert!(
  %UploaderApi.Packages.Content{
    package_id: package.id,
    content: "binary_stuff_for_package_five"
  }
)

package = UploaderApi.Repo.insert!(
  %UploaderApi.Packages.Package{
    #    path: Const.storage_bucket,
    filename: "some_filename",
    description: "some description",
    name: "name4",
    version: "1.5.0"
  }
)
UploaderApi.Repo.insert!(
  %UploaderApi.Packages.Content{
    package_id: package.id,
    content: "binary_stuff_for_package_six"
  }
)

package = UploaderApi.Repo.insert!(
  %UploaderApi.Packages.Package{
    #    path: Const.storage_bucket,
    filename: "some_filename",
    description: "some description",
    name: "name4",
    version: "1.6.1"
  }
)

UploaderApi.Repo.insert!(
  %UploaderApi.Packages.Content{
    package_id: package.id,
    content: "binary_stuff_for_package_seven"
  }
)

# This file is responsible for configuring your application
# and its dependencies with the aid of the Mix.Config module.
#
# This configuration file is loaded before any dependency and
# is restricted to this project.
use Mix.Config

# General application configuration
config :uploader_api,
  ecto_repos: [UploaderApi.Repo]

# Configures the endpoint
config :uploader_api, UploaderApiWeb.Endpoint,
  url: [host: "localhost"],
  secret_key_base: "JFdlb2MzGLVokgJx6nYqPU10cYgY5dtnDmmf9//rztuGoUqDC/XNCFSNEcE9LxT4",
  render_errors: [view: UploaderApiWeb.ErrorView, accepts: ~w(json)],
  pubsub: [name: UploaderApi.PubSub,
           adapter: Phoenix.PubSub.PG2]

# Configures Elixir's Logger
config :logger, :console,
  format: "$time $metadata[$level] $message\n",
  metadata: [:request_id]

# Import environment specific config. This must remain at the bottom
# of this file so it overrides the configuration defined above.
import_config "#{Mix.env}.exs"

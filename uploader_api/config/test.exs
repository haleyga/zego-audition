use Mix.Config

# We don't run a server during test. If one is required,
# you can enable the server option below.
config :uploader_api,
       UploaderApiWeb.Endpoint,
       http: [
         port: 4001
       ],
       server: false

# Print only warnings and errors during test
config :logger, level: :warn

# Configure your database
config :uploader_api,
       UploaderApi.Repo,
       adapter: Ecto.Adapters.Postgres,
       username: System.get_env("POSTGRES_USER") || "postgres",
       password: System.get_env("POSTGRES_PASSWORD") || "postgres",
       database: System.get_env("POSTGRES_DB") || "zego_packages_test",
       hostname: System.get_env("POSTGRES_HOST") || "localhost",
       pool: Ecto.Adapters.SQL.Sandbox

config :uploader_api,
       :storage_bucket,
       "./.tmp"

config :uploader_api,
       :dummy_api_key,
       "i_am_a_dummy"

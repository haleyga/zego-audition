defmodule UploaderApi.Packages do
  @moduledoc """
  The Packages context.
  """

  import Ecto.Query, warn: false, only: [ from: 2 ]

  alias UploaderApi.Repo

  alias UploaderApi.Packages.{ Package, Content }

  @doc """
  Returns the list of packages.

  ## Examples

      iex> list_packages(%{})
      [%Package{}, ...]

  """
  def list_packages(), do: list_packages([ ])
  def list_packages(params) when is_map(params) do
    with filters <- params
                    |> Enum.map(fn { k, v } -> take_if_allowed(k, v) end)
                    |> Enum.reduce([ ], fn x, acc -> acc ++ x end)
      do

      list_packages(filters)
    end
  end

  def list_packages(filters) when is_list(filters) do
    with query <- Package do

      maybe_filtered_query = if (filters[ :name ]),
                                do: query
                                    |> name_filter(filters[ :name ]),
                                else: query

      Repo.all(maybe_filtered_query)
    end
  end

  @allowed_index_params ~w(name)
  defp take_if_allowed(key, value, allowed \\ @allowed_index_params) do
    if (Enum.member?(allowed, key))do
      [ { :"#{key}", value } ]
    else
      [ ]
    end
  end


  #  NOTE: I think the like/2 function is still vulnerable to LIKE-injection, so this should be carefully considered for a real app.
  defp name_filter(query, name_kernel) do
    with str <- "%#{name_kernel}%" do
      from p in query, where: like(p.name, ^str)
    end
  end

  @doc """
  Gets a single package.

  Raises `Ecto.NoResultsError` if the Package does not exist.

  ## Examples

      iex> get_package!(123)
      %Package{}

      iex> get_package!(456)
      ** (Ecto.NoResultsError)

  """

  def get_package!({ name, version }), do: Repo.get_by!(Package, name: name, version: version)

  def get_package!({ name }) do
    with query <- from p in Package,
                       where: p.name == ^name,
                       order_by: [
                         desc: p.inserted_at
                       ],
                       limit: 1
      do

      Repo.one!(query)
    end
  end

  def get_package!(id), do: Repo.get!(Package, id)

  @doc """
  Creates a package.

  ## Examples

      iex> create_package(%{field: value})
      {:ok, %Package{}}

      iex> create_package(%{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def create_package(attrs \\ %{ }) do
    with _dir <- Application.get_env(:uploader_api, :storage_bucket)
      do

      %Package{ }
      |> Package.changeset(attrs)
      |> Repo.insert()
    end
  end

  @doc """
  Updates a package.  Only allow updates to the description of a package.

  ## Examples

      iex> update_package(package, %{field: new_value})
      {:ok, %Package{}}

      iex> update_package(package, %{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def update_package(%Package{ } = package, %{ description: description }) do
    package
    |> change_package()
    |> Ecto.Changeset.put_change(:description, description)
    |> Repo.update()
  end

  @doc """
  Disallow updates to other fields by ignoring anything other than 'description'.
  """
  def update_package(%Package{ } = package, _attrs) do
    { :ok, package }
  end

  @doc """
  Deletes a Package.

  ## Examples

      iex> delete_package(package)
      {:ok, %Package{}}

      iex> delete_package(package)
      {:error, %Ecto.Changeset{}}

  """
  def delete_package(%Package{ } = package) do
    Repo.delete(package)
  end

  @doc """
  Returns an `%Ecto.Changeset{}` for tracking package changes.

  ## Examples

      iex> change_package(package)
      %Ecto.Changeset{source: %Package{}}

  """
  def change_package(%Package{ } = package) do
    Package.changeset(package, %{ })
  end

  def get_content!(package_id) do
    with query <- from c in Content, where: c.package_id == ^package_id do
      Repo.one!(query)
    end
  end

  def create_content(attrs \\ %{ }) do
    %Content{ }
    |> Content.changeset(attrs)
    |> Repo.insert()
  end

  def delete_content(%Content{ } = content) do
    Repo.delete(content)
  end
end

defmodule UploaderApi.Packages.Content do
  use Ecto.Schema
  import Ecto.Changeset

  @required ~w(package_id content)a

  schema "contents" do
    field :content, :string

    belongs_to :package, UploaderApi.Packages.Package

    timestamps()
  end

  @doc false
  def changeset(package, attrs) do
    package
    |> cast(attrs, @required)
    |> validate_required(@required)
    |> foreign_key_constraint(:package_id)
  end
end

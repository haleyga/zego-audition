defmodule UploaderApi.Packages.Package do
  use Ecto.Schema
  import Ecto.Changeset

  @storage_bucket Application.get_env(:uploader_api, :storage_bucket)

  @required ~w(name version filename)a
  @optional ~w(description)a

  schema "packages" do
    field :name, :string
    field :version, :string
    field :filename, :string
    field :path, :string, default: @storage_bucket
    field :description, :string

    timestamps()
  end

  @doc false
  def changeset(package, attrs) do
    package
    |> cast(attrs, @required ++ @optional)
    |> validate_required(@required)
    |> unique_constraint(:version, name: :packages_name_version_unique_index)
  end
end

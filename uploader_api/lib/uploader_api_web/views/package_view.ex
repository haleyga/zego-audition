defmodule UploaderApiWeb.PackageView do
  use UploaderApiWeb, :view
  alias UploaderApiWeb.PackageView

  def render("index.json", %{ packages: packages }) do
    %{ data: render_many(packages, PackageView, "package.json") }
  end

  def render("show.json", %{ package: package }) do
    %{ data: render_one(package, PackageView, "package.json") }
  end

  def render("package.json", %{ package: package }) do
    %{
      id: package.id,
      name: package.name,
      version: package.version,
      description: package.description,
      filename: package.filename
    }
  end

  def render("content.json", %{ content: content }) do
    %{
      id: content.id,
      package_id: content.package_id,
      content: content.content
    }
  end
end

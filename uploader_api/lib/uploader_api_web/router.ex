defmodule UploaderApiWeb.Router do
  use UploaderApiWeb, :router

  import UploaderApiWeb.AuthPlugs

  pipeline :api do
    plug :accepts, [ "json" ]
    plug :has_auth_token
  end

  scope "/", UploaderApiWeb do
    pipe_through :api

    get "/packages/:name/latest", PackageController, :latest
    get "/packages/:name/:version", PackageController, :specific

    post "/packages/upload", PackageController, :upload
    post "/packages/download", PackageController, :download

    get "/packages/:name", PackageController, :versions

    resources "/packages", PackageController, except: [ :new, :show, :edit ]

  end
end

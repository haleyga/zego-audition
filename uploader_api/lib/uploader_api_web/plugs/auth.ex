defmodule UploaderApiWeb.AuthPlugs do
  import Plug.Conn

  def has_auth_token(conn), do: has_auth_token(conn, %{ })
  def has_auth_token(conn, _) do
    with headers <- conn.req_headers,
         { _header_key, api_key } <- Enum.find(headers, fn { k, _v } -> k == "x-dummy-api-key" end),
         true <- api_key == Application.get_env(:uploader_api, :dummy_api_key) do
      conn
    else
      _ ->
        conn
        |> resp(401, "Unauthorized")
        |> halt()
    end
  end
end

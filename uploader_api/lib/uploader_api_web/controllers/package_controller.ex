defmodule UploaderApiWeb.PackageController do
  use UploaderApiWeb, :controller

  alias UploaderApi.Packages
  alias UploaderApi.Packages.{ Package, Content }

  action_fallback UploaderApiWeb.FallbackController

  def index(conn, params) do
    packages = Packages.list_packages(params)
    render(conn, "index.json", packages: packages)
  end

  @doc """
  Fetch a latest version of a package.
  """
  def versions(conn, %{ "name" => name }) do
    with packages <- Packages.list_packages(name: name) do
      render(conn, "index.json", packages: packages)
    end
  end

  def latest(conn, %{ "name" => name }) do
    with package <- Packages.get_package!({ name }) do
      render(conn, "show.json", package: package)
    end
  end

  def specific(conn, %{ "name" => name, "version" => version }) do
    with package <- Packages.get_package!({ name, version }) do
      render(conn, "show.json", package: package)
    end
  end

  def create(conn, %{ "package" => package_params }) do
    with { :ok, %Package{ } = package } <- Packages.create_package(package_params) do
      conn
      |> put_status(:created)
      |> put_resp_header("location", "/packages/#{package_params[ "name" ]}")
      |> render("show.json", package: package)
    end
  end

  @doc """
  Allow updates to description only.
  """
  def update(
        conn,
        %{
          "id" => id,
          "package" => %{
            "description" => description
          }
        }
      ) do

    package = Packages.get_package!(id)

    with { :ok, %Package{ } = package } <- Packages.update_package(package, %{ description: description }) do
      render(conn, "show.json", package: package)
    end
  end

  @doc """
  Catch any other update attempts and just return the current package.
  """
  def update(conn, %{ "id" => id, "package" => _package_params }),
      do: render(conn, "show.json", package: Packages.get_package!(id))

  def delete(conn, %{ "id" => id }) do
    package = Packages.get_package!(id)
    with { :ok, %Package{ } } <- Packages.delete_package(package) do
      send_resp(conn, :no_content, "")
    end
  end

  def download(conn, %{ "package_id" => package_id }) do
    with content <- Packages.get_content!(package_id) do
      render(conn, "content.json", content: content)
    end
  end

  def upload(conn, %{ "package_id" => _package_id, "content" => _content } = attrs) do
    with { :ok, %Content{ } = _content } <- Packages.create_content(attrs) do
      send_resp(conn, :created, "")
    end
  end
end

defmodule UploaderApiWeb.PackageControllerTest do
  use UploaderApiWeb.ConnCase

  alias UploaderApi.Packages
  alias UploaderApi.Packages.{ Package, Content }

  @storage_bucket Application.get_env(:uploader_api, :storage_bucket)

  @create_attrs %{
    #    path: @storage_bucket,
    name: "some_name",
    version: "some_version",
    filename: "some_filename",
    description: "some description"
  }
  @valid_update_attrs %{
    description: "some updated description"
  }
  @invalid_update_attrs %{
    #    path: @storage_bucket,
    name: "some updated name",
    version: "some updated version",
    filename: "some updated filename"
  }
  @invalid_attrs %{
    #    path: nil,
    name: nil,
    version: nil,
    filename: nil,
    description: nil
  }

  def fixture(:package) do
    { :ok, package } = Packages.create_package(@create_attrs)
    package
  end

  setup %{ conn: conn } do
    { :ok, conn: put_req_header(conn, "accept", "application/json") }
  end

  defp temp_test_seeds() do
    with base <- %{
      path: @storage_bucket,
      filename: "some_filename",
      description: "some description"
    }
      do

      Packages.create_package(Map.merge(base, %{ name: "name1", version: "1.0.0" }))
      Packages.create_package(Map.merge(base, %{ name: "name1", version: "1.1.0" }))
      Packages.create_package(Map.merge(base, %{ name: "name1", version: "2.0.0" }))
      Packages.create_package(Map.merge(base, %{ name: "name2", version: "0.0.1" }))
      Packages.create_package(Map.merge(base, %{ name: "name3", version: "1.0.0" }))
      Packages.create_package(Map.merge(base, %{ name: "name4", version: "1.5.0" }))
      Packages.create_package(Map.merge(base, %{ name: "name4", version: "1.6.1" }))
    end
  end

  describe "auth" do
    test "requests should fail without api key", %{ conn: conn } do
      conn = get conn, package_path(conn, :index)
      assert response(conn, 401)
    end
  end

  describe "index" do

    test "lists all packages", %{ conn: conn } do
      with conn <- add_auth_header(conn) do
        conn = get conn, package_path(conn, :index)
        assert json_response(conn, 200)[ "data" ] == [ ]
      end
    end

    test "correctly filters index results", %{ conn: conn } do
      temp_test_seeds()

      # 1
      conn = build_conn()
             |> add_auth_header()

      data = conn
             |> get(package_path(conn, :index, %{ name: "name1" }))
             |> json_response(200)
             |> Map.fetch!("data")
      assert length(data) == 3

      # 2
      data = conn
             |> get(package_path(conn, :index, %{ name: "name2" }))
             |> json_response(200)
             |> Map.fetch!("data")
      assert length(data) == 1

      # 3
      data = conn
             |> get(package_path(conn, :index, %{ name: "name" }))
             |> json_response(200)
             |> Map.fetch!("data")
      assert length(data) == 7
    end
  end

  describe "retrieve specific package" do
    test "renders available versions when name is given", %{ conn: conn } do
      with conn <- add_auth_header(conn) do
        temp_test_seeds()

        conn = get(conn, package_path(conn, :versions, "name1"))
        assert length(json_response(conn, 200)[ "data" ]) == 3
      end
    end

    test "renders correct package when latest version is requested", %{ conn: conn } do
      with conn <- add_auth_header(conn) do
        temp_test_seeds()

        conn = get conn, "/packages/name1/latest"
        %{ "id" => left_id } = left = json_response(conn, 200)[ "data" ]
        assert left == %{
                 "id" => left_id,
                 "name" => "name1",
                 "version" => "2.0.0",
                 "description" => "some description",
                 "filename" => "some_filename"
               }
      end
    end

    test "renders correct package when name and version are requested", %{ conn: conn } do
      with conn <- add_auth_header(conn) do
        temp_test_seeds()

        conn = get conn, "/packages/name1/1.0.0"
        %{ "id" => left_id } = left = json_response(conn, 200)[ "data" ]
        assert left == %{
                 "id" => left_id,
                 "name" => "name1",
                 "version" => "1.0.0",
                 "description" => "some description",
                 "filename" => "some_filename"
               }
      end
    end
  end

  describe "create package" do
    test "renders package when data is valid", %{ conn: conn } do
      with conn <- add_auth_header(conn) do
        conn = post conn, package_path(conn, :create), package: @create_attrs
        assert %{ "id" => id } = json_response(conn, 201)[ "data" ]

        conn = build_conn()
               |> add_auth_header()
               |> get("/packages/some_name/some_version")

        assert json_response(conn, 200)[ "data" ] == %{
                 "id" => id,
                 "name" => "some_name",
                 "version" => "some_version",
                 "description" => "some description",
                 "filename" => "some_filename"
               }
      end
    end

    test "renders errors when data is invalid", %{ conn: conn } do
      with conn <- add_auth_header(conn) do
        conn = post conn, package_path(conn, :create), package: @invalid_attrs
        assert json_response(conn, 422)[ "errors" ] != %{ }
      end
    end
  end

  describe "update package" do
    setup [ :create_package ]

    @tag :me
    test "renders package when data is valid", %{ conn: conn, package: %Package{ id: id } = package } do
      with conn <- add_auth_header(conn) do
        conn = put conn, package_path(conn, :update, package), package: @valid_update_attrs
        assert %{ "id" => ^id } = json_response(conn, 200)[ "data" ]

        conn = build_conn()
               |> add_auth_header()
               |> get("/packages/#{package.name}/#{package.version}")

        assert json_response(conn, 200)[ "data" ] == %{
                 "id" => id,
                 "name" => "some_name",
                 "version" => "some_version",
                 "description" => "some updated description",
                 "filename" => "some_filename"
               }
      end
    end

    test "renders original package when data is invalid", %{ conn: conn, package: %Package{ id: id } = package } do
      with conn <- add_auth_header(conn) do
        conn = put conn, package_path(conn, :update, package), package: @invalid_update_attrs
        assert json_response(conn, 200)[ "data" ] == %{
                 "id" => id,
                 "name" => "some_name",
                 "version" => "some_version",
                 "description" => "some description",
                 "filename" => "some_filename"
               }
      end
    end
  end

  describe "delete package" do
    setup [ :create_package ]

    test "deletes chosen package", %{ conn: conn, package: package } do
      with conn <- add_auth_header(conn) do
        conn = delete conn, package_path(conn, :delete, package)
        assert response(conn, 204)
        assert_error_sent 404, fn ->
          build_conn()
          |> add_auth_header()
          |> get("/packages/#{package.name}/#{package.version}")
        end
      end
    end
  end

  describe "upload/download package file content" do
    setup [ :create_package ]

    test "uploads content to contents table and downloads same content",
         %{
           conn: conn,
           package: %Package{
             id: package_id
           }
         } do
      with conn <- add_auth_header(conn) do
        conn = post conn, "/packages/upload", %{ package_id: package_id, content: "content_for_you" }
        assert response(conn, :created)

        conn = build_conn()
               |> add_auth_header()
               |> post("/packages/download", %{ package_id: package_id })

        response = json_response(conn, 200)
        assert response[ "package_id" ] == package_id
        assert response[ "content" ] == "content_for_you"
      end
    end

    test "cannot upload new content once content is set for a package",
         %{
           conn: conn,
           package: %Package{
             id: package_id
           }
         } do
      with %Content{ id: _content_id, content: _content, package_id: package_id } <- Packages.create_content(
        %{ content: "content_for_you", package_id: package_id }
      ) do

        conn = post conn, "/packages/upload", %{ package_id: package_id, content: "content_for_me" }

        conn = post conn, "/packages/download", %{ package_id: package_id }
        response = json_response(conn, 200)
        assert response[ "package_id" ] == package_id
        assert response[ "content" ] == "content_for_you"
      end
    end
  end

  defp create_package(_) do
    package = fixture(:package)
    { :ok, package: package }
  end

  defp add_auth_header(conn) do
    conn
    |> put_req_header("x-dummy-api-key", Application.get_env(:uploader_api, :dummy_api_key))
  end
end

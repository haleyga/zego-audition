defmodule UploaderApi.PackagesTest do
  use UploaderApi.DataCase

  alias UploaderApi.Packages

  @storage_bucket Application.get_env(:uploader_api, :storage_bucket)

  describe "packages" do
    alias UploaderApi.Packages.Package

    @valid_attrs %{
      path: @storage_bucket,
      name: "some_name",
      version: "some_version",
      filename: "some_filename",
      description: "some description"
    }
    @valid_update_attrs %{
      description: "some updated description"
    }
    @invalid_update_attrs %{
      path: @storage_bucket,
      name: "some updated name",
      version: "some updated version",
      filename: "some updated filename"
    }
    @invalid_attrs %{ path: nil, name: nil, version: nil, filename: nil, description: nil }

    def package_fixture(attrs \\ %{ }) do
      { :ok, package } =
        attrs
        |> Enum.into(@valid_attrs)
        |> Packages.create_package()

      package
    end

    test "list_packages/0 returns all packages" do
      package = package_fixture()
      assert Packages.list_packages() == [ package ]
    end

    test "get_package!/1 returns the package with given id" do
      package = package_fixture()
      assert Packages.get_package!(package.id) == package
    end

    test "get_package!/1 returns the package with given name and version" do
      package = package_fixture()
      assert Packages.get_package!({ package.name, package.version }) == package
    end

    test "get_package!/1 returns the latest package with the given name" do
      package1 = package_fixture()
      package2 = package_fixture(%{ name: package1.name, version: "10.0.0.0" })
      assert Packages.get_package!({ package1.name }) == package2
    end

    test "create_package/1 with valid data creates a package" do
      assert { :ok, %Package{ } = package } = Packages.create_package(@valid_attrs)
      assert package.name == "some_name"
      assert package.version == "some_version"
    end

    test "create_package/1 with invalid data returns error changeset" do
      assert { :error, %Ecto.Changeset{ } } = Packages.create_package(@invalid_attrs)
    end

    test "update_package/2 with valid data updates the package" do
      package = package_fixture()
      assert { :ok, package } = Packages.update_package(package, @valid_update_attrs)
      assert %Package{ } = package
      assert package.name == "some_name"
      assert package.version == "some_version"
      assert package.filename == "some_filename"
      assert package.description == "some updated description"
    end

    test "update_package/2 with invalid data returns same package" do
      package = package_fixture()
      assert { :ok, package } = Packages.update_package(package, @invalid_update_attrs)
      assert %Package{ } = package
      assert package.name == "some_name"
      assert package.version == "some_version"
      assert package.filename == "some_filename"
      assert package.description == "some description"
    end

    test "delete_package/1 deletes the package" do
      package = package_fixture()
      assert { :ok, %Package{ } } = Packages.delete_package(package)
      assert_raise Ecto.NoResultsError, fn -> Packages.get_package!(package.id) end
    end

    test "change_package/1 returns a package changeset" do
      package = package_fixture()
      assert %Ecto.Changeset{ } = Packages.change_package(package)
    end
  end
end
